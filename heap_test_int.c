#include "heap_int.h"
#include <stdio.h>

int main ( int argc, char **argv) {

	int i=0, c;
	heap h;
	huff_tree temp;
	heap_init(&h, 4);
	while(c != 4) {
		printf("Choose an operation:\n");
		printf("1) Insert a number.\n");
		printf("2) Extract the minimum.\n");
		printf("3) Display the heap\n");
		printf("4) Quit.\n");
		printf("Choice:");
		scanf("%d", &c);
		switch(c) {
			case 1:
				printf("Enter a number:");
				scanf("%d", &i);

				temp.freq = i;
				temp.byte = 'z';
				heap_insert(&h, temp);
			break;
			case 2:
				temp = heap_extract_min(&h);
				printf("extracted : (%d,%c)\n", temp.freq, temp.byte);
			break;
			case 3:
				printf("Heap:\n");
				heap_debug_print(&h);
				printf("\n");
				break;
		};
	}

}
