#ifndef HEAP_H
#define HEAP_H

/*
#include "huffman.h"
 */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
	int freq;
	char byte;
}huff_tree;

typedef struct { 
	int size;
	int max_size;
	huff_tree *arr;
} heap;

void heap_init( heap *, int );
void heap_sift_down( heap *, int);
void heap_sift_up( heap *, int);
huff_tree heap_extract_min(heap *);
void heap_expand( heap *);
void heap_insert( heap *, huff_tree);
void heap_debug_print(heap *);
#endif
