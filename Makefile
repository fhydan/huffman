hc : hc.c huffman.o heap.o
	gcc -o hc hc.c huffman.o heap.o verbose.o

hc_mmap: hc_mmap.c huffman.o heap.o
	gcc -o hc_mmap hc_mmap.c huffman.o heap.o verbose.o

hc_fread: hc_fread.c huffman.o heap.o
	gcc -o hc_fread hc_fread.c huffman.o heap.o verbose.o

hc_getc: hc_getc.c huffman.o heap.o
	gcc -o hc_getc hc_getc.c huffman.o heap.o verbose.o

huffman.o: huffman.c huffman.h verbose.o
	gcc -c huffman.c

heap.o: heap.c heap.h verbose.o
	gcc -c heap.c

verbose.o: verbose.c verbose.h
	gcc -c verbose.c
clean:
	rm hc hc_mmap hc_fread hc_getc *.o
