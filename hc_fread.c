#include "huffman.h"
#include "heap.h"
#include "verbose.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

#define ALPH_SIZE 256
#define HDR_SIZE 9
#define BLOCK_SIZE 1024

void print_frequencies(int freq[]) {
	for(int i = 0; i < ALPH_SIZE; i++)
		if( freq[i] )
		verbose("%X:%d\n", i, freq[i]);
	verbose("\n");	
}
void print_codes( int freq[], code codes[] ) {
	for(int i = 0; i < ALPH_SIZE; i++)
		if( freq[i] )
		verbose("%X:%X:%d\n", i, codes[i].bits & codes[i].mask, freq[i]);
	verbose("\n");	
}

heap *make_heap(int freq[], code codes[], int more_than_one) {
	heap *h = malloc( sizeof(heap));
	heap_init(h, 1);
	for(int i = 0; i < ALPH_SIZE; i++){
		if(freq[i] > 0 
		        || ( more_than_one && i == 0)) {
			huff_tree *temp = malloc(sizeof(huff_tree));
			huff_init(temp, i,freq[i]);
			heap_insert(h, temp);
			verbose("\n");
		}
	}
	return h;
}

huff_tree *build_huffman_tree( int freq[], code codes[], heap *h) {
	huff_tree *h1, *h2;
	while( h->size > 1 ) {
		h1 = heap_extract_min(h);
		h2 = heap_extract_min(h);
		huff_tree *m = huff_merge( h1, h2, codes );
		heap_insert(h, m);
//		heap_debug_print(h);
	}
	return heap_extract_min(h);
}

/**/
void write_tree(FILE *dst, huff_tree *t) {
	int byte_count = huff_tree_size(t) * 2;
	unsigned char z=0, o=1;

	if(t->zero == NULL) {
		verbose("leaf:%X\n", t->byte);
		fwrite(&o, 1,1,dst);
		fwrite( &(t->byte), 1, 1, dst);
	} else {
		verbose("non-leaf\n");
		fwrite(&z,1,1,dst);
		fwrite(&z,1,1,dst);
		write_tree(dst, t->zero);
		write_tree(dst, t->one);
	}
}

/**/
int write_compressed(FILE *src, FILE *dst, code codes[], int *pad_size
    , int *compressed_size) {
	unsigned char carry, c;
	unsigned int read_bytes=0, buf_bytes=0
	    ,rdy_bits = 0,rem_bits =0
		,bits,mask, i=0 , m,x, write_count=0 ;
    unsigned char buffer[BLOCK_SIZE];
    unsigned char write_buf[BLOCK_SIZE * 2];    //Risky heurestic?! Must revise.

	rewind(src);
	while(1){
		read_bytes = fread(buffer, sizeof(char), BLOCK_SIZE, src);
		if(read_bytes == 0) {
			verbose("Read 0 bytes. Must be EOF\n");
			break;
		}
		verbose("Read %d bytes\n", read_bytes);
        /*Write*/
        i = 0;
        while(1) {
            if(rem_bits == 0) {
                c = buffer[i];
                i += 1;
                if ( i > read_bytes)
                    break;
                bits = codes[c].bits;
                mask = codes[c].mask;

                for(rem_bits=0;mask>>rem_bits;rem_bits++);

                for(unsigned int j = 0x8000; j >0;j = j >> 1)
                    if(mask & j)
                        verbose("%d", (bits & j) != 0);
                verbose("\t\t\tencoding: %X as %X\n", c, bits);
            }
            verbose("remaining:%d\t:readybits=%d\n", rem_bits, rdy_bits);
            m = rem_bits < 8-rdy_bits ? rem_bits : 8-rdy_bits;
            
            /*Copy m bits from 'bits' to carry*/
            carry = (carry<<m) + (bits>> (rem_bits-m));
            bits = bits - (bits >> (rem_bits-m) << (rem_bits-m));

            rdy_bits += m;
            rem_bits -= m;

            if(rdy_bits == 8) {
                write_buf[ buf_bytes ] = carry;
                carry =0;
                buf_bytes += 1;
                rdy_bits = 0;
                verbose("comitting:");

                
                if(buf_bytes == BLOCK_SIZE) {
                    for(int j =0 ;j < buf_bytes;j++) {
                        verbose("writing:");
                        for(unsigned char k = 0x80;k>0;k = k >> 1)
                            verbose("%d", (write_buf[j] & k) != 0);
                        verbose("\n");
                    }
                    x = fwrite(write_buf, sizeof(char), buf_bytes, dst);
                    if ( x != buf_bytes) {
                        fprintf(stderr, "Could only write %d out of %d bytes\n"
                                , x, buf_bytes);
                        return write_count+x;
                    }
                    write_count += x;
                    buf_bytes = 0;
                }
            }
        }
        /*No more bytes read. Flush write buffer*/
        if(buf_bytes > 0) {
            for(int j =0 ;j < buf_bytes;j++) {
                verbose("writing: %x\t", write_buf[j]);
                for(unsigned char k = 0x80;k>0;k = k >> 1)
                    verbose("%d", (write_buf[j] & k) != 0);
                verbose("\n");
            }
            x = fwrite(write_buf, sizeof(char), buf_bytes, dst);
            if( x != buf_bytes) {
                fprintf(stderr, "Could only write %d out of %d bytes\n"
                        , x, buf_bytes);
                return write_count+x;
            }
            write_count += x;
            buf_bytes = 0;
        }
    }
    verbose("After writing loop: readybits=%d\tm=%d\n",rdy_bits,m); 
        /*Padding needed?*/
	if(rdy_bits > 0 ){ 
		carry = carry << (8-rdy_bits);
		write_count += 1;
		fwrite(&carry, 1, 1, dst);
		*pad_size = 8-rdy_bits;
        verbose("Writing:");
		for(unsigned char i = 0x80;i>0;i = i >> 1)
			verbose("%d", (carry & i ) != 0);
        verbose("\n");
    }
	*compressed_size = write_count;
}

void to_bytes(int n, unsigned char buffer[]) {
	buffer[0] = (n >> 24) & 0xFF;
	buffer[1] = (n >> 16) & 0xFF;
	buffer[2] = (n >> 8) & 0xFF;
	buffer[3] = n & 0xFF;
}

void write_header(FILE *dst, int file_size, int tree_size, int pad_size) {
	int old_pos = ftell(dst);
	rewind(dst);
	unsigned char buffer[4];
	to_bytes(file_size, buffer);
	fwrite(buffer,1,4,dst);
	to_bytes(tree_size, buffer);
	fwrite(buffer,1,4,dst);
	to_bytes(pad_size, buffer);
	fwrite(buffer+3,1,1,dst);
	fseek(dst, old_pos, SEEK_SET);
}


void compress_file(FILE *src, FILE *dst){
	int freq[ALPH_SIZE] = {0}, tree_size=0, pad_size=0, compressed_size=0;
	code codes[ALPH_SIZE] = {0};
	unsigned char buffer[4];
	int c, more_than_one=0;;
	huff_tree *t;
	heap *h;
	FILE *devnull = fopen("/dev/null", "wb");

	while( (c = fgetc(src)) != EOF) {
//		printf("%X\n", c);
		freq[c] += 1;
        if(freq[c] > 1)
            more_than_one=1;
	}

	print_frequencies(freq);
	h = make_heap(freq, codes, more_than_one);
	heap_debug_print(h);
	t = build_huffman_tree(freq, codes, h);
	verbose("Codes after merging:\n");
	print_codes(freq, codes);

	/*calculate total tree size*/
	tree_size = huff_tree_size(t) * 2;

	fseek(dst, HDR_SIZE, SEEK_SET);
	write_tree(dst, t);
	write_compressed(src, dst, codes, &pad_size, &compressed_size);
	write_header(dst, tree_size + compressed_size + HDR_SIZE
			, tree_size, pad_size);
	verbose("Header:\n%d\n%d\n%d\n", tree_size+compressed_size+HDR_SIZE
			, tree_size, pad_size);

}
/* =========================================================================
 * Decoding functionns
 * =========================================================================*/
huff_tree *read_tree( FILE *src ) {
	unsigned char b[2];
   	fread(b, 2, 1, src);
	huff_tree *node = malloc(sizeof(huff_tree));
	if(b[0])
		node->byte = b[1];
	else {
		node->zero = read_tree(src);
		node->one = read_tree(src);
	}

	return node;
}

huff_tree * process_bits (FILE *out, unsigned char byte, unsigned char used_bits
		, huff_tree *tree, huff_tree *p ) {

	unsigned char mask, init_mask = (unsigned char) 1 << 7;
    verbose(",");
	if(used_bits < 1)
		return p;

 //  	mask = 1 << (used_bits-1);
    mask = (unsigned char) 1 << 7;
	for(mask = init_mask; mask > (init_mask >> used_bits) ; mask = mask >> 1) {
        verbose("%d", (byte & mask) != 0);
        if( byte & mask )
			p = p->one;
		else
			p = p->zero;

		if( p->one == NULL ) {
            verbose("\tDecoded:%x\n", p->byte);
            fwrite(&(p->byte), 1, 1, out);
			p = tree;
		}
	}
	verbose("\n");
	return p;
}

void decompress_file(FILE *in, FILE *out) {
	huff_tree *tree, *p;
	int pad_size=0, i=0;
	long int filesize=0, compressed_size, rem_bytes, read_bytes=0;
	unsigned char c, mask=0x80;
	unsigned char buffer[ BLOCK_SIZE ];

	fseek(in, 0, SEEK_END);
	filesize = ftell(in);

	fseek(in, 8, SEEK_SET);
	fread(&c, 1, 1, in);
	pad_size = c;
	verbose("Padding size:%d\n", pad_size);

	fseek(in, HDR_SIZE,SEEK_SET);
	p = tree = read_tree(in);
	compressed_size = filesize - ftell(in);
	verbose("File Size:%d\nCompressed size:%d\n", filesize, compressed_size);

	rem_bytes = compressed_size;
	while(1) {
		read_bytes = fread(buffer, 1, BLOCK_SIZE, in);
		if(read_bytes == 0 ) {
		    verbose("Read 0 bytes\n");
			break;
        }
		rem_bytes -= read_bytes;

		i = 0;

		for(i=0; i < read_bytes-1;i++) {
			p = process_bits(out, buffer[i], 8, tree, p);
		}
		if(rem_bytes)
			p = process_bits(out, buffer[i], 8, tree, p);
		else {
			p = process_bits(out, buffer[i], 8 - pad_size, tree, p);
            verbose("No remaining bytes\n");
            break;
        }
    }
}



/*
 *
 */
int main( int argc, char ** argv) {

	int c,i;
	int encode_mode=0;
	int decode_mode=0;

	if(argc < 4) {
		fprintf(stderr, "Incorrect number of arguments.\n");
		return 0;
	}


	for(i=1; i < argc && argv[i][0] == '-' ;i++) {
		switch(argv[i][1]) {
			case 'c':
				encode_mode =1;
				break;
			case 'd':
				decode_mode =1;
				break;
			case 'v':
				set_verbose(1);
				break;
		}
	}

	if(encode_mode == 1 && decode_mode == 1) {
		fprintf(stderr, "Cannot use -c and -d arguments");
		return -1;
	}
	else if (encode_mode == 1) {
		for(i=1;i<argc && argv[i][0] == '-';i++);
		verbose("Opening file %s for reading\n", argv[i]);
		FILE *in = fopen(argv[i], "rb");
		if( in == NULL ) {
			fprintf(stderr, "Error:\n");
			return -1;
		}
		for(i+=1;i<argc && argv[i][0] == '-'; i++);
		verbose("Opening file %s for writing\n", argv[i]);
		FILE *out = fopen(argv[i], "wb");
		if( out == NULL ) {
			fprintf(stderr, "Error:\n");
			return -1;
		}
		compress_file(in, out);
		fclose(in);
		fclose(out);
	} else if (decode_mode == 1) {
		for(i=1;i < argc && argv[i][0] == '-';i++);
		verbose("Opening file %s for reading\n", argv[i]);
		FILE *in = fopen(argv[i], "rb");
		if( in == NULL) {
			fprintf(stderr, "Error:\n");
			return -1;
		}
		for(i+=1;i<argc && argv[i][0] == '-';i++);
		verbose("Opening file %s for writing\n", argv[i]);
		FILE *out = fopen(argv[i], "wb" );
		if( out == NULL ) {
			fprintf(stderr, "Error:\n");
			return -1;
		}
		decompress_file(in, out);
		fclose(in);
		fclose(out);
	}

	return 0;
}
