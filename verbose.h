extern int ver_flag;
#ifndef VERBOSE_H
#define VERBOSE_H

#include <stdarg.h>
#include <stdio.h>


int verbose ( const char * restrict , ... ) ;
void set_verbose(int);

#endif
