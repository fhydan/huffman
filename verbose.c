#ifndef VERBOSE_H
#define VERBOSE_H

#include <stdarg.h>
#include <stdio.h>

int ver_flag = 0;

int verbose ( const char * restrict format, ... ) {
	if (!ver_flag)
		return 0;

	va_list args;
	va_start(args, format);
	int ret = vprintf(format, args);
	va_end(args);

	return ret;
}
void set_verbose(int x) {
	ver_flag = x;
}
#endif
