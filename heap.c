#include "heap.h"
/* Size must be power of 2 */
void heap_init( heap *h, int size ) {
		h->arr = malloc(sizeof( huff_tree *) * (size+1) );
		h->size = 0;
		h->max_size = size;
}

void heap_sift_down( heap *h , int i) {
	if(h->size == 0 )
		return;

	while( 2 * i <= h->size) {
		int min;
		if( ( 2 * i + 1 <= h->size) 
				&&  h->arr[i*2]->freq < h->arr[i*2 + 1]->freq)
			min = i*2;
		else
			min = i * 2 + 1;

		if( h->arr[i]->freq > h->arr[min]->freq ) {
			huff_tree *t = h->arr[i];
			h->arr[i] = h->arr[min];
			h->arr[min] = t;
		}
		i = min;
	}
}

void heap_sift_up( heap *h, int i ) {
	if(h->size == 0 )
		return;

	while( i > 1 ) {
		if( h->arr[i/2]->freq > h->arr[i]->freq ) {
			huff_tree *t = h->arr[i];
			h->arr[i] = h->arr[i/2];
			h->arr[i/2] = t;
		}
		i = i / 2;
	}
}

huff_tree *heap_extract_min( heap *h ) {
	huff_tree *min;
	if(h->size == 0)
		return NULL;

	min = h->arr[1];
	h->arr[1] = h->arr[ h->size ];
	h->size -= 1;
	heap_sift_down(h, 1);
	
	return min;
}

void heap_expand( heap *h ) {
	h->max_size = h->max_size * 2;
	huff_tree **arr2 = malloc( sizeof( huff_tree *) * (h->max_size + 1)  );
	memcpy( arr2, h->arr, (h->size+1) * sizeof(huff_tree *) );
	free(h->arr);
	h->arr = arr2;
}

void heap_insert( heap *h, huff_tree *t) {
	h->size++;
	if( h->size == h->max_size )
		heap_expand( h );

	h->arr[h->size] = t;
	heap_sift_up(h, h->size);
}

void heap_debug_print(heap *h) {
	for(int i = 1; i <= h->size; i++) {
		verbose("( %d, %X ), ", h->arr[i]->freq, h->arr[i]->byte);
	}
}
