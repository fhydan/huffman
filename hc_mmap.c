#include "huffman.h"
#include "heap.h"
#include "verbose.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <unistd.h>

#define ALPH_SIZE 256
#define HDR_SIZE 9
#define BLOCK_SIZE 1024

void print_frequencies(int freq[]) {
	for(int i = 0; i < ALPH_SIZE; i++)
		if( freq[i] )
		verbose("%X:%d\n", i, freq[i]);
	verbose("\n");	
}
void print_codes( int freq[], code codes[] ) {
	for(int i = 0; i < ALPH_SIZE; i++)
		if( freq[i] )
		verbose("%X:%X:%d\n", i, codes[i].bits & codes[i].mask, freq[i]);
	verbose("\n");	
}

heap *make_heap(int freq[], code codes[], int more_than_one) {
	heap *h = malloc( sizeof(heap));
	heap_init(h, 1);
	for(int i = 0; i < ALPH_SIZE; i++){
		if(freq[i] > 0 
		        || ( more_than_one && i == 0)) {
			huff_tree *temp = malloc(sizeof(huff_tree));
			huff_init(temp, i,freq[i]);
			heap_insert(h, temp);
			verbose("\n");
		}
	}
	return h;
}

huff_tree *build_huffman_tree( int freq[], code codes[], heap *h) {
	huff_tree *h1, *h2;
	while( h->size > 1 ) {
		h1 = heap_extract_min(h);
		h2 = heap_extract_min(h);
//		printf("Merging %d with %d\n", h1->freq, h2->freq);
		huff_tree *m = huff_merge( h1, h2, codes );
		heap_insert(h, m);
//		printf("Heap contents:");
//		heap_debug_print(h);
//		printf("\n");
	}
	return heap_extract_min(h);
}

/**/
void write_tree(FILE *dst, huff_tree *t) {
	int byte_count = huff_tree_size(t) * 2;
	unsigned char z=0, o=1;

	if(t->zero == NULL) {
		verbose("leaf:%X\n", t->byte);
		fwrite(&o, 1,1,dst);
		fwrite( &(t->byte), 1, 1, dst);
	} else {
		verbose("non-leaf\n");
		fwrite(&z,1,1,dst);
		fwrite(&z,1,1,dst);
		write_tree(dst, t->zero);
		write_tree(dst, t->one);
	}
}

/**/
int write_compressed(const unsigned char * in_mapped, int in_size
        , FILE *dst, code codes[], int *pad_size
        , int *compressed_size) {
	int c;
	unsigned char carry;
	unsigned int readybits = 0,codebits = 0,remaining =0
		,bits,mask, m, write_count=0, i = 0 ;

	while(1){
		if(remaining == 0) {
		    if(i >= in_size)
		        break;
		    c = in_mapped[i];

            bits = codes[c].bits;
			mask = codes[c].mask;
			for(unsigned int i = 0x8000;i>0;i=i>>1)
				if( mask & i )
					verbose("%d", (bits & i) != 0 );
			verbose("\t\t\tencoding: %X as %X\n", c, bits);

			for(codebits=0; mask>>codebits != 0; codebits++);
			remaining = codebits;
		}
		verbose("remaining:%d\t:readybits=%d\n", remaining, readybits);
		if( remaining > 8-readybits )
			m = 8-readybits;
		else
			m = remaining;

		carry = (carry<<m) + (bits>> (codebits-m));
		bits = bits - ( bits >> (codebits-m) << (codebits-m) ) ;
		readybits += m;
		remaining -= m;
		codebits -= m;

		if(readybits == 8) {
			write_count++;
			fwrite(&carry, 1, 1, dst);
			verbose("Writing:");

			for(unsigned char i = 0x80;i>0;i = i >> 1)
				verbose("%d", (carry & i ) != 0);
			verbose("\n");
			carry = 0;
			readybits = 0;
		}
	}
	verbose("After writing loop: readybits=%d\tm=%d\n",readybits,m); 
	if(readybits > 0 ){ 
		carry = carry << (8-readybits);
		write_count++;
		fwrite(&carry, 1, 1, dst);
		*pad_size = 8-readybits;
        verbose("Writing:");
		for(unsigned char i = 0x80;i>0;i = i >> 1)
			verbose("%d", (carry & i ) != 0);
        verbose("\n");
    }
	*compressed_size = write_count;
}

void to_bytes(int n, unsigned char buffer[]) {
	buffer[0] = (n >> 24) & 0xFF;
	buffer[1] = (n >> 16) & 0xFF;
	buffer[2] = (n >> 8) & 0xFF;
	buffer[3] = n & 0xFF;
}

void write_header(FILE *dst, int file_size, int tree_size, int pad_size) {
	int old_pos = ftell(dst);
	rewind(dst);
	unsigned char buffer[4];
	to_bytes(file_size, buffer);
	fwrite(buffer,1,4,dst);
	to_bytes(tree_size, buffer);
	fwrite(buffer,1,4,dst);
	to_bytes(pad_size, buffer);
	fwrite(buffer+3,1,1,dst);
	fseek(dst, old_pos, SEEK_SET);
}


void compress_file(const unsigned char *in_mapped, int in_size, FILE *dst){
	int freq[ALPH_SIZE] = {0}, tree_size=0, pad_size=0, compressed_size=0;
	code codes[ALPH_SIZE] = {0};
	unsigned char buffer[4];
	int c, more_than_one=0;;
	huff_tree *t;
	heap *h;

    for(int i =0; i < in_size;i++){
        c = in_mapped[i];
        //		printf("%X\n", c);
		freq[c] += 1;
        if(freq[c] > 1)
            more_than_one=1;
	}

	print_frequencies(freq);
	h = make_heap(freq, codes, more_than_one);
	heap_debug_print(h);
	t = build_huffman_tree(freq, codes, h);
	verbose("Codes after merging:\n");
	print_codes(freq, codes);

	/*calculate total tree size*/
	tree_size = huff_tree_size(t) * 2;

	fseek(dst, HDR_SIZE, SEEK_SET);
	write_tree(dst, t);
	write_compressed(in_mapped, in_size, dst, codes, &pad_size, &compressed_size);
	write_header(dst, tree_size + compressed_size + HDR_SIZE
			, tree_size, pad_size);
	verbose("Header:\n%d\n%d\n%d\n", tree_size+compressed_size+HDR_SIZE
			, tree_size, pad_size);

}
/* =========================================================================
 * Decoding functionns
 * =========================================================================*/
huff_tree *read_tree(const unsigned char *in_mapped, int in_size , int offset ) {
    if( offset >= in_size) {
        fprintf(stderr, "Overflow at read_tree:\n");
        return NULL;
    }  
	huff_tree *node = malloc(sizeof(huff_tree));
	if(in_mapped[offset++])
		node->byte = in_mapped[offset++];
	else {
		node->zero = read_tree(in_mapped, in_size,offset);
		node->one = read_tree(in_mapped, in_size,offset);
	}

	return node;
}

huff_tree * process_bits (FILE *out, unsigned char byte, unsigned char used_bits
		, huff_tree *tree, huff_tree *p ) {

	unsigned char mask, init_mask = (unsigned char) 1 << 7;
    verbose(",");
	if(used_bits < 1)
		return p;

 //  	mask = 1 << (used_bits-1);
    mask = (unsigned char) 1 << 7;
	for(mask = init_mask; mask > (init_mask >> used_bits) ; mask = mask >> 1) {
        verbose("%d", (byte & mask) != 0);
        if( byte & mask )
			p = p->one;
		else
			p = p->zero;

		if( p->one == NULL ) {
            verbose("\tDecoded:%x\n", p->byte);
            fwrite(&(p->byte), 1, 1, out);
			p = tree;
		}
	}
	verbose("\n");
	return p;
}

void decompress_file(const unsigned char *in_mapped, int in_size, FILE *out) {
	huff_tree *tree, *p;
	int pad_size=0, i=0,j=0;
	long int filesize=0, compressed_size, rem_bytes, read_bytes=0;
	unsigned char c, mask=0x80;
	unsigned char buffer[ BLOCK_SIZE ];

	pad_size = in_mapped[8];
	verbose("Padding size:%d\n", pad_size);

	p = tree = read_tree(in_mapped, in_size,HDR_SIZE);
	compressed_size = in_size - ((int *) in_mapped)[1];
	verbose("File Size:%d\nCompressed size:%d\n", in_size, compressed_size);

	rem_bytes = compressed_size;
	for(j=HDR_SIZE;j < in_size-1;j++) {
		p = process_bits(out, in_mapped[j], 8, tree, p);
    }
    p = process_bits(out, in_mapped[j],8-pad_size,tree,p);

}



/*
 *
 */
int main( int argc, char ** argv) {

    FILE *out;
	int c,i,src_fd, dst_fd, status, in_size;
	struct stat s;
	const unsigned char *in_mapped;
	int encode_mode=0;
	int decode_mode=0;

	if(argc < 4) {
		fprintf(stderr, "Incorrect number of arguments.\n");
		return 0;
	}


	for(i=1; i < argc && argv[i][0] == '-' ;i++) {
		switch(argv[i][1]) {
			case 'c':
				encode_mode =1;
				break;
			case 'd':
				decode_mode =1;
				break;
			case 'v':
				set_verbose(1);
				break;
		}
	}

	if(encode_mode == 1 && decode_mode == 1) {
		fprintf(stderr, "Cannot use -c and -d arguments");
		return -1;
	}
	else if (encode_mode == 1) {

		for(i=1;i<argc && argv[i][0] == '-';i++);
		verbose("Opening file %s for reading\n", argv[i]);
		src_fd = open(argv[i], O_RDONLY);
		if( src_fd < 0 ) {
			fprintf(stderr, "Error: %s\n", strerror(errno));
			return -1;
		}
		status = fstat(src_fd, &s);
		if( status < 0) {
            fprintf(stderr, "Error at fstat: %s\n", strerror(errno));
            return -1;
		}
		in_size = s.st_size;
		in_mapped = mmap(0,in_size, PROT_READ, MAP_PRIVATE, src_fd, 0);
		if( in_mapped == MAP_FAILED) {
            fprintf(stderr, "mmap failed %s\n", strerror(errno));
            return -1;
		}

        for(i+=1;i<argc && argv[i][0] == '-'; i++);
		verbose("Opening file %s for writing\n", argv[i]);
		out = fopen(argv[i], "wb");;
		if( out == NULL) {
			fprintf(stderr, "Error: %s\n", strerror(errno));
			return -1;
		}
		compress_file(in_mapped, in_size, out);
        munmap((void *)in_mapped, in_size);
        close(src_fd);
        fclose(out);

    } else if (decode_mode == 1) {
		for(i=1;i < argc && argv[i][0] == '-';i++);
		verbose("Opening file %s for reading\n", argv[i]);
		src_fd = open(argv[i], O_RDONLY);
		if(src_fd < 0) {
			fprintf(stderr, "Error: %s\n", strerror(errno));
			return -1;
		}

        status = fstat(src_fd, &s);
		if (status < 0 ) {
            fprintf(stderr, "Error at fstat: %s\n", strerror(errno));
            return -1;
		}

		in_size = s.st_size;
		in_mapped = mmap(0, in_size,PROT_READ,MAP_PRIVATE,src_fd,0);

		if( in_mapped == MAP_FAILED) {
            fprintf(stderr,  "mmap failed %s\n", strerror(errno));
            return -1;
		}
		for(i+=1;i<argc && argv[i][0] == '-';i++);
		verbose("Opening file %s for writing\n", argv[i]);
		FILE *out = fopen(argv[i], "wb" );
		if( out == NULL ) {
			fprintf(stderr, "Error:\n");
			return -1;
		}
		decompress_file(in_mapped, in_size, out);
		munmap((void *)in_mapped, in_size);
		close(src_fd);
		fclose(out);
	}

	return 0;
}
