#ifndef HUFFMAN_H
#define HUFFMAN_H

#include "verbose.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct huff_tree_node {
	int freq;
	unsigned char byte;
	struct huff_tree_node *zero, *one, *parent;
} huff_tree;

typedef struct code {
	unsigned int bits;
	unsigned int mask;
} code;

int huff_tree_size(huff_tree *);
void huff_init(huff_tree *,unsigned char, int freq);
huff_tree *huff_merge(huff_tree *, huff_tree *,code []);
void huff_update_tree_codes(huff_tree*, code [], unsigned char);
char decode(huff_tree *, code);
code encode(code [], unsigned char);

#endif
