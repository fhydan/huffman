#include "huffman.h"

int huff_tree_size( huff_tree *t ) {
	if(t == NULL )
		return 0;
	return 1 + huff_tree_size(t->zero) + huff_tree_size(t->one);
}

void print_bits(code c) {
	verbose("bits:%X\t:",c.bits); 
	for(int i=0x8000;i>0;i=i>>1) 
		verbose("%d ", (c.bits & i) != 0 );
	verbose("\n");
	verbose("mask:%X\t:", c.mask);
	for(int i=0x8000;i>0;i=i>>1) 
		verbose("%d ", (c.mask & i) != 0);
	verbose("\n");
}

void huff_init( huff_tree *t,unsigned char c, int freq ) {
	t->freq = freq;
	t->byte = c;
	t->zero = t->one = t->parent = NULL;
}

void huff_update_tree_codes(huff_tree *root, code codes[], unsigned char bit ) {

	if( root->zero == NULL && root->one == NULL ) {
		int newm = codes[ root->byte].mask + 1;
		verbose("Code for:%X\n", root->byte);
		if(newm == 0 ) {
			fprintf(stderr, "ERROR: bitmask overflow \n");
			return;
		}
		codes[ root->byte ].bits = codes[ root->byte ].bits + (bit? newm:0);
		codes[ root->byte ].mask = codes[root->byte].mask |= newm;
		print_bits(codes[root->byte] );
		return;
	}

	huff_update_tree_codes(root->zero, codes, bit);
	huff_update_tree_codes(root->one, codes, bit);
}

huff_tree *huff_merge( huff_tree *t1, huff_tree *t2, code codes[]) {
	huff_tree *merged = malloc(sizeof(huff_tree));
	merged->freq = t1->freq + t2->freq;
	merged->byte = 0;

	t1->parent = merged;
	t2->parent = merged;

	if(t1->freq >= t2->freq){
		merged->one = t1;
		merged->zero = t2;
		huff_update_tree_codes(t1, codes, 1);	
		huff_update_tree_codes(t2,codes, 0);
	} else {
		merged->zero = t1;
		merged->one = t2;
		huff_update_tree_codes(t1, codes, 0);	
		huff_update_tree_codes(t2,codes, 1);
	}
		return merged;
}

code encode( code codes[], unsigned char byte) {
	return codes[byte];
}

char decode(huff_tree *t, code enc){
	huff_tree *p = NULL;

	int m = enc.mask + 1;		/*First relevant bit in enc.bits*/
	/*for(m = 0x80; !(m & enc.mask); m >> 1);*/


	while(t != NULL ) {
		p = t;
		if( enc.bits & m) {
			t = t->one;
		} else
			t = t->zero;
		m = m >> 1;
	}
	return p->byte;
}
