#!/bin/sh
if [ $# -ne 2 ]
then
    printf "Wrong number of arguments\nUsage: $0 <binary> <directory of tests>"
    exit 
fi
for TEST in $(find $2 -name "test*")
do
	TEST_NAME=$(echo $TEST | awk -F "/" '{ print $NF }')
	printf "Running test :$TEST_NAME at $TEST\n"
	printf "Compression: "
	S=$(/usr/bin/time -f "%E" ./hc -c $TEST $2/${TEST_NAME}.hc | awk -F "\n" '{print $1}')
	printf "$S seconds\tDecompression: "
	S=$(/usr/bin/time -f "%E" ./hc -d $2/${TEST_NAME}.hc $2/${TEST_NAME}.dc | awk -F "\n" '{print $1}')
	printf "$S seconds\n"
	DIFF=$(diff $TEST $2/${TEST_NAME}.dc)
	if [ "$DIFF" != "" ]
	then
	    printf "[Failed test: $TEST]\n"
	else
	    printf "[Passed test: $TEST]\n"
    fi
done

rm $2/test*.hc $2/test*.dc
